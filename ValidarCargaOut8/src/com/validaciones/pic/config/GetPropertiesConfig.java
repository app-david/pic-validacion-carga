package com.validaciones.pic.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class GetPropertiesConfig {

	String result = "";
	InputStream inputStream;
	
	private static Logger logger = Logger.getLogger(GetPropertiesConfig.class);
 
	public Properties getPropValues() throws IOException {
		Properties prop = null;
		try {
			prop = new Properties();
			String propFileName = "config.properties";
 
			File parent = new File(GetPropertiesConfig.class.getProtectionDomain().getCodeSource().getLocation().toURI());
			File file = new File(parent.getParentFile()+"\\"+propFileName);
			
			inputStream = new FileInputStream(file);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
		} catch (Exception e) {
			logger.error("Exception al obtener el archivo de configuracion: " , e);
		} finally {
			inputStream.close();
		}
		return prop;
	}
	
}
