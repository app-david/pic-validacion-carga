package com.validaciones.pic.job;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.validaciones.pic.config.GetPropertiesConfig;
import com.validaciones.pic.sftp.SFTPConnector;
import com.validaciones.pic.vo.FileCargaVO;
import com.validaciones.pic.ws.RestService;
import java.util.Calendar;

public class ValidarCargasOutJob implements Job {

	private static final String CTR_ASIG_PROS_ = "CTR_ASIG_PROS_";
	private static final String ASIG_PROSPECTO_ = "ASIG_PROSPECTO_";
	private static final String FEEDBACK_ACUMU_ = "FEEDBACK_ACUMU_";
	private static final String CTR_FEED_ACUMU_ = "CTR_FEED_ACUMU_";
	private static final String CONTROL_FEEDBACK_ = "CONTROL_FEEDBACK_";
	private static final String FEEDBACK_ = "FEEDBACK_";
	private static final String CONTROL_BITACORA_ = "CONTROL_BITACORA_";
	private static final String BITACORA_ = "BITACORA_";
	private static final String VERSION = "_V1.TXT";
	private static final String JOBACUMU = "A1JP5555";
	private static final String HENVIOACUM = "00:00";
	private static final String JOB = "PPICTP001";
	private static final String HORAJOB = "21:00";
	private static final String VERSION1 = "V1";
	private static final String OK = "OK";
	private static final String NOK = "NO OK";
	private static final String CORREGIR = "CORREGIR VERSI�N";
	private static final String PEND = "EN PROCESO";
	private static final String GEN = "GENERADO OK";
	private static final String FIN_BITACORA = "FINALIZ� BITACORA";
	private static final String FIN_FEEDBACK = "FINALIZ� FEEDBACK";
	private static final String FIN_ASIG_PROSPECTO = "FINALIZ� PROSPECTO";
	private static final String FIN_FEEDBACK_ACUMU = "FINALIZ� FEEDBACK ACUMULADO";
	
	private static final String USERNAME = "userkjid";
	private static final String HOST = "150.250.242.33";
	private static final int PORT = 22;
	private static final String PASSWORD = "admin123";
	
	private static Logger logger = Logger.getLogger(ValidarCargasOutJob.class);

	@SuppressWarnings("deprecation")
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		//Fijar fecha y hora manual
//		Calendar cal = Calendar.getInstance(); // creates calendar
//	    cal.setTime(new Date()); // sets calendar time/date
//	   // cal.add(Calendar.HOUR_OF_DAY, 18); // adds one hour
//	    cal.add(Calendar.DAY_OF_MONTH, -1);
//	    Date now = cal.getTime();
		
//		//Fecha y hora Actual

		Date now = new Date();
		
		GetPropertiesConfig propertiesConfig = new GetPropertiesConfig();
		
		try {
			this.properties = propertiesConfig.getPropValues();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		int horaActual = now.getHours();
//		int diaActual = now.getDay();
//		logger.info("dia de ejecucion ["+diaActual+"]");
//		if(diaActual == 0 || diaActual == 6) {
//			logger.info("no se ejecuta el proceso por ser d�a sabado o domingo");
//			return;
//		}
		String[] arrHoras = this.properties.getProperty("config.rango.horas").split(",");
		int horaInicioEjecuacion = Integer.parseInt(arrHoras[0]);
		int horaFinEjecuacion = Integer.parseInt(arrHoras[1]);
		if (horaActual >= horaInicioEjecuacion && horaActual < horaFinEjecuacion) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdfLabel = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
			//joana
//		    Calendar calendar = Calendar.getInstance();
//		    calendar.add(Calendar.DATE, -1);
//		    Date yesterday = calendar.getTime();
//		    String valorFechaFile =  sdf.format(yesterday); 
			String valorFechaFile =  sdf.format(now); 
			
			logger.info("Iniciando envio de files ["+valorFechaFile+"]");
			
			try {
				SFTPConnector sshConnector = new SFTPConnector();
				sshConnector.connect(USERNAME, PASSWORD, HOST, PORT);
				List<FileCargaVO> filesTemp = sshConnector.listFiles(this.properties.getProperty("config.dir.out"), "*.TXT");

				List<FileCargaVO> files = new ArrayList<FileCargaVO>();
				List<FileCargaVO> filesVal = new ArrayList<FileCargaVO>();

				if (files != null) {
					boolean ctrlBit = false;
					boolean ctrlPros = false;
					boolean ctrlFeed = false;
					boolean ctrlAcum = false;
					for (FileCargaVO fileCargaVO : filesTemp) {
						if (fileCargaVO.getFileName().indexOf(CTR_ASIG_PROS_ + valorFechaFile) >= 0
								|| fileCargaVO.getFileName().indexOf(ASIG_PROSPECTO_ + valorFechaFile) >= 0
								|| fileCargaVO.getFileName().indexOf(FEEDBACK_ACUMU_ + valorFechaFile) >= 0
								|| fileCargaVO.getFileName().indexOf(CTR_FEED_ACUMU_ + valorFechaFile) >= 0
								|| fileCargaVO.getFileName().indexOf(CONTROL_FEEDBACK_ + valorFechaFile) >= 0
								|| fileCargaVO.getFileName().indexOf(FEEDBACK_ + valorFechaFile) >= 0
								|| fileCargaVO.getFileName().indexOf(CONTROL_BITACORA_ + valorFechaFile) >= 0
								|| fileCargaVO.getFileName().indexOf(BITACORA_ + valorFechaFile) >= 0) {
								
							int posFinal = fileCargaVO.getFileName().length();
							int lengthArchivo = posFinal - 15;
							int lengthVersion = posFinal - 6;
							String archivoBase = fileCargaVO.getFileName().substring(0, lengthArchivo);
							String version = fileCargaVO.getFileName().substring(lengthVersion, lengthVersion + 2);
							
								if (archivoBase.equals(FEEDBACK_ACUMU_)|| archivoBase.equals(CTR_FEED_ACUMU_)){
									fileCargaVO.setJobEjecutaEnvio(JOBACUMU);
									fileCargaVO.setHoraEnvio(HENVIOACUM);
								}else{
									fileCargaVO.setJobEjecutaEnvio(JOB);
									fileCargaVO.setHoraEnvio(HORAJOB);
								}
								
								fileCargaVO.setEstadoProceso(PEND);
								
								if(version.equals(VERSION1)){
								   fileCargaVO.setEstadoArchivo(OK);
								}else{
								   fileCargaVO.setEstadoArchivo(NOK);
								}
								
								if (archivoBase.equals(CONTROL_BITACORA_)){
									if(fileCargaVO.getEstadoArchivo().equals(OK)){
										fileCargaVO.setEstadoProceso(FIN_BITACORA);	
									}else{
										fileCargaVO.setEstadoProceso(CORREGIR);
									}
									ctrlBit= true;
								}
									
								if (archivoBase.equals(CONTROL_FEEDBACK_)){
									if(fileCargaVO.getEstadoArchivo().equals(OK)){
										fileCargaVO.setEstadoProceso(FIN_FEEDBACK);
									}else{
										fileCargaVO.setEstadoProceso(CORREGIR);
									}
									ctrlFeed = true;
								}
									
								if (archivoBase.equals(CTR_ASIG_PROS_)){
									if(fileCargaVO.getEstadoArchivo().equals(OK)){
										fileCargaVO.setEstadoProceso(FIN_ASIG_PROSPECTO);
									}else{
										fileCargaVO.setEstadoProceso(CORREGIR);
									}
									ctrlPros=true;
								}
									
								if (archivoBase.equals(CTR_FEED_ACUMU_)){
									if(fileCargaVO.getEstadoArchivo().equals(OK)){
										fileCargaVO.setEstadoProceso(FIN_FEEDBACK_ACUMU);
									}else{
										fileCargaVO.setEstadoProceso(CORREGIR);
									}
									ctrlAcum = true;
								}
								
							filesVal.add(fileCargaVO);
						}
					}
					
					for (FileCargaVO fileCargaVO : filesVal) {
						
						int posFinal = fileCargaVO.getFileName().length();
						int lengthArchivo = posFinal - 15;
						String archivoBase = fileCargaVO.getFileName().substring(0, lengthArchivo);
						
						if (archivoBase.equals(BITACORA_)&& ctrlBit){
							if(fileCargaVO.getEstadoArchivo().equals(OK)){
								fileCargaVO.setEstadoProceso(FIN_BITACORA);
							}else{
								fileCargaVO.setEstadoProceso(CORREGIR);
							}
						}
						
						if (archivoBase.equals(FEEDBACK_)&& ctrlFeed){
							if(fileCargaVO.getEstadoArchivo().equals(OK)){
								fileCargaVO.setEstadoProceso(FIN_FEEDBACK);
							}else{
								fileCargaVO.setEstadoProceso(CORREGIR);
							}
						}
						
						if (archivoBase.equals(ASIG_PROSPECTO_)&& ctrlPros){
							if(fileCargaVO.getEstadoArchivo().equals(OK)){
								fileCargaVO.setEstadoProceso(FIN_ASIG_PROSPECTO);
							}else{
								fileCargaVO.setEstadoProceso(CORREGIR);
							}
						}
						
						if (archivoBase.equals(FEEDBACK_ACUMU_)&& ctrlAcum){
							if(fileCargaVO.getEstadoArchivo().equals(OK)){
								fileCargaVO.setEstadoProceso(FIN_FEEDBACK_ACUMU);
							}else{
								fileCargaVO.setEstadoProceso(CORREGIR);
							}
						}
						
						files.add(fileCargaVO);
					}
				}

				String cadenaHtml = "";
				if(files.size() > 0) {
					cadenaHtml = "<p>Los siguientes archivos se encontraron a la fecha: " + sdfLabel.format(now) + "</p>";
					cadenaHtml += "<table style=\"border:0px;\" cellspacing=\"0\">"
							+ "<tr>"
							    + this.buildHeadTable("#")
								+ this.buildHeadTable("Nombre del archivo")
								+ this.buildHeadTable("Tama�o del archivo")
								+ this.buildHeadTable("�ltima modificaci�n")
								+ this.buildHeadTable("Job de Transmisi�n")
								+ this.buildHeadTable("Hora de Transmisi�n")
								+ this.buildHeadTable("Estado del Archivo")
							+ "</tr>";
					int numLinea = 1;
					for (FileCargaVO fileCargaVO : files) {
						String estado = null;
						if(fileCargaVO.getEstadoProceso().equals(PEND)){
							estado = this.buildRowTablePen(fileCargaVO.getEstadoProceso());
						}else{
							estado = (fileCargaVO.getEstadoArchivo().equals(OK) ? this.buildRowTableOK(fileCargaVO.getEstadoProceso()): this.buildRowTableWarning(fileCargaVO.getEstadoProceso()));
						}
						cadenaHtml +="<tr>"
								        + this.buildRowTable(String.valueOf(numLinea)) 
										+ this.buildRowTable(fileCargaVO.getFileName())
										+ this.buildRowTable(String.valueOf(fileCargaVO.getSize()))
										+ this.buildRowTable(this.parseFechaArchivo(fileCargaVO.getFechaCreacion()))
										+ this.buildRowTable(fileCargaVO.getJobEjecutaEnvio())
										+ this.buildRowTable(fileCargaVO.getHoraEnvio())
										+ estado
									+ "</tr>";
						numLinea++;
					}
					cadenaHtml += "</table>";
				}else {
					cadenaHtml = "<p>No se encontraron archivos hasta el momento: " + sdfLabel.format(now) + "</p>";
				}
				
				this.sendMail(cadenaHtml);

			} catch (IOException e) {
				logger.error("IOException",e);
			} catch (IllegalAccessException e) {
				logger.error("IllegalAccessException:",e);
			} catch (JSchException e) {
				logger.error("JSchException:",e);
			} catch (SftpException e) {
				logger.error("SftpException:",e);
			}
		}else {
			logger.info("Ejecucion fuera de rango, debe ser >= "+horaInicioEjecuacion+" && < "+horaFinEjecuacion+" horas");
		}

	}

	private String buildRowTable(String rowValue) {
		return "<td style=\"background-color:#fff;padding:8px;color:#000;border-bottom:1px #ccc solid;\">"+(rowValue)+"</td>";
	}
	
	private String buildRowTableWarning(String rowValueWarning) {
		return "<td style=\"background-color:#F70505;padding:8px;color:#f5f9f9f5;border-bottom:1px #ccc solid;\">"+(rowValueWarning)+"</td>";
	}

	private String buildRowTableOK(String rowValueOK) {
		return "<td style=\"background-color:#05f739;padding:8px;color:#0c0c0cf5;border-bottom:1px #ccc solid;\">"+(rowValueOK)+"</td>";
	}
	
	private String buildRowTablePen(String rowValuePen) {
		return "<td style=\"background-color:#f3f705;padding:8px;color:#0c0c0cf5;border-bottom:1px #ccc solid;\">"+(rowValuePen)+"</td>";
	}
	
	private String buildHeadTable(String valueHead) {
		return "<th style=\"background-color:black;border-top:1px #ccc solid;border-bottom:1px #ccc solid;padding:8px;color:#fff;text-align:left;\">"+(valueHead)+"</th>";
	}

	private String parseFechaArchivo(String fechaCreacion) {
		SimpleDateFormat formatUS = new SimpleDateFormat("EE MMM dd hh:mm:ss z yyyy",Locale.US);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
		try {
			Date fecha = formatUS.parse(fechaCreacion);
			return format.format(fecha);
		} catch (ParseException e) {
			return fechaCreacion;
		}
	}

	private String getTextFechaNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
		Date now = new Date();
		String textNow = sdf.format(now);
		return textNow;
	}
	
	public void sendMail(String cadenaHtml) throws IOException {
			
			RestService service = new RestService();
			String asunto = "Estado de carga nocturna - "+this.getTextFechaNow();
			service.createMessage(cadenaHtml, asunto, this.properties.getProperty("config.email.destino"));
			
	}
	
	private Properties properties;
	public Properties getProperties() {	return properties; }
	public void setProperties(Properties properties) { this.properties = properties; }

}
