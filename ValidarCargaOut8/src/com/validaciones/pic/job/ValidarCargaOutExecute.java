package com.validaciones.pic.job;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.validaciones.pic.config.GetPropertiesConfig;

public class ValidarCargaOutExecute {
	private static Logger logger = Logger.getLogger(ValidarCargaOutExecute.class);
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		logger.info("Iniciando Job!");
		GetPropertiesConfig propertiesConfig = new GetPropertiesConfig();
		Properties properties = propertiesConfig.getPropValues();
		logger.info("Cron value: "+properties.getProperty("config.cron.rule"));
	    SchedulerFactory schedulerFactory=new StdSchedulerFactory();
	    Scheduler scheduler= schedulerFactory.getScheduler();
	    JobDetail jobDetail=new JobDetailImpl("myJobClass","myJob1",ValidarCargasOutJob.class);
	    CronTrigger trigger=new CronTriggerImpl("cronTrigger","myJob1",properties.getProperty("config.cron.rule"));
	    scheduler.scheduleJob(jobDetail,trigger);
	    scheduler.start();
	    logger.info("Job Iniciado de forma correcta!");
	}
}
