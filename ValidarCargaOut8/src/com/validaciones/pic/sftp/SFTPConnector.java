package com.validaciones.pic.sftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.validaciones.pic.vo.FileCargaVO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Clase encargada de establecer conexion y ejecutar comandos SFTP.
 */
public class SFTPConnector {

	private Session session;

	public void connect(String username, String password, String host, int port)
			throws JSchException, IllegalAccessException {
		if (this.session == null || !this.session.isConnected()) {
			JSch jsch = new JSch();

			this.session = jsch.getSession(username, host, port);
			this.session.setPassword(password);

			// Parametro para no validar key de conexion.
			this.session.setConfig("StrictHostKeyChecking", "no");

			this.session.connect();
		} else {
			throw new IllegalAccessException("Sesion SFTP ya iniciada.");
		}
	}

	public final void disconnect() {
		this.session.disconnect();
	}

	@SuppressWarnings("unchecked")
	public List<FileCargaVO> listFiles(String path,String patron) throws JSchException, SftpException {
		List<FileCargaVO> listFilesCargas = new ArrayList<FileCargaVO>();
		if (this.session != null && this.session.isConnected()) {
			FileCargaVO fileCargaVO = null;
			ChannelSftp channelSftp = (ChannelSftp) this.session.openChannel("sftp");
			channelSftp.connect();
			channelSftp.cd(path);
			Vector<ChannelSftp.LsEntry> list = channelSftp.ls(patron);
			for (ChannelSftp.LsEntry entry : list) {
				fileCargaVO = new FileCargaVO();
				fileCargaVO.setFileName(entry.getFilename());
				fileCargaVO.setSize(entry.getAttrs().getSize());
				fileCargaVO.setFechaCreacion(entry.getAttrs().getMtimeString());
				listFilesCargas.add(fileCargaVO);
			}
		}
		return listFilesCargas;
	}
}
