package com.validaciones.pic.ws;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.validaciones.pic.config.GetPropertiesConfig;
import com.validaciones.pic.domain.Mensaje;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

public class RestService {
	
	private static Logger logger = Logger.getLogger(RestService.class);

	public RestService() throws IOException {
		GetPropertiesConfig propertiesConfig = new GetPropertiesConfig();
		this.properties = propertiesConfig.getPropValues();
	}
	
	public void createMessage(String html,String asunto, String destinatario) {
		int proxyPort = Integer.parseInt(this.properties.getProperty("config.proxy.port"));
		String proxyHost = this.properties.getProperty("config.proxy.url");
		final String username = this.properties.getProperty("config.proxy.user");
		final String password = this.properties.getProperty("config.proxy.password");
		
		Authenticator proxyAuthenticator = new Authenticator() {
			
			@Override
			public Request authenticate(Route route, Response response) throws IOException {
				String credential = Credentials.basic(username, password);
				return response.request().newBuilder()
				           .header("Proxy-Authorization", credential)
				           .build();
			}
		};
		
		Proxy proxyTest = new Proxy(Proxy.Type.HTTP,new InetSocketAddress(proxyHost, proxyPort));
		
		OkHttpClient.Builder builder = new OkHttpClient.Builder().proxy(proxyTest).proxyAuthenticator(proxyAuthenticator);
		
		OkHttpClient client = builder.build();
		logger.info("Enviado email al destinatario: "+destinatario);
		MediaType mediaType = MediaType.parse("application/json");
		Mensaje mensaje = new Mensaje();
		mensaje.setAsunto(asunto);
		mensaje.setMensaje(html);
		mensaje.setTo(destinatario);
		Gson gson = new Gson();
		RequestBody body = RequestBody.create(mediaType,  gson.toJson(mensaje));
		Request request = new Request.Builder()
		  .url(this.properties.getProperty("config.emaul.ws.url"))
		  .post(body)
		  .addHeader("Token-Acceso", this.properties.getProperty("config.emaul.ws.token"))
		  .addHeader("Content-Type", "application/json")
		  .build();
		try {
			Response response = client.newCall(request).execute();
			logger.info("Se notifico a: "+destinatario+", con status: "+response.code()+" con fecha: "+ new Date());
		} catch (IOException e) {
			logger.error("Error al enviar el correo a "+ destinatario, e);
		}
		
	}
	
	private Properties properties;
	public Properties getProperties() {	return properties; }
	public void setProperties(Properties properties) { this.properties = properties; }

}
