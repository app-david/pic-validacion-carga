package com.validaciones.pic.vo;

import java.io.Serializable;

public class FileCargaVO implements Serializable{

	private static final long serialVersionUID = -5853813363417859912L;

	private String fileName;
	public String getFileName() { return fileName; }
	public void setFileName(String fileName) { this.fileName = fileName; }
	
	private Long size;
	public Long getSize() { return size; }
	public void setSize(Long size) { this.size = size; }
	
	private String fechaCreacion;
	public String getFechaCreacion() { return fechaCreacion; }
	public void setFechaCreacion(String fechaCreacion) { this.fechaCreacion = fechaCreacion; }
	
	private String jobEjecutaEnvio;
	public String getJobEjecutaEnvio() {return jobEjecutaEnvio;}
	public void setJobEjecutaEnvio(String jobEjecutaEnvio) {this.jobEjecutaEnvio = jobEjecutaEnvio;}
	
	private String horaEnvio;
	public String getHoraEnvio() {return horaEnvio;}
	public void setHoraEnvio(String horaEnvio) {this.horaEnvio = horaEnvio;}
	
	private String estadoArchivo;
	public String getEstadoArchivo() {return estadoArchivo;}
	public void setEstadoArchivo(String estadoArchivo) {this.estadoArchivo = estadoArchivo;}
	
	private String estadoProceso;
	public String getEstadoProceso() {return estadoProceso;}
	public void setEstadoProceso(String estadoProceso) {this.estadoProceso = estadoProceso;}
	
}
